# ChhoeTaigi 找台語

### 台文版簡介
「ChhoeTaigi 揣台語」是1 ê得著g0v獎助金ê計畫，主要beh解決現此時學台語ē tú--tio̍h siōng大ê困難：Beh chhōe台語ê字詞，辭典真oh thang chhōe ē tio̍h。Mā整理台語文ê字詞資料庫，hō͘想beh應用ê人koh khah利便。

計畫網站：
https://grants.g0v.tw/projects/5a631883900e86001bf9f4d7

台語字詞資料庫：
https://github.com/ChhoeTaigi/ChhoeTaigiDatabase


### 華文版簡介
「ChhoeTaigi 找台語」是個得到g0v獎助金的計畫，主要要解決現在學台語時會碰到最大的困難：要找台語的字詞時，辭典很難找得到。也同時整理台語文的字詞資料庫，讓想要應用的人更加方便。

計畫網站：
https://grants.g0v.tw/projects/5a631883900e86001bf9f4d7

台語字詞資料庫：
https://github.com/ChhoeTaigi/ChhoeTaigiDatabase
